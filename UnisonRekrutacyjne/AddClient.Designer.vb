﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddClient
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.AddressLB = New System.Windows.Forms.Label()
        Me.AddressTB = New System.Windows.Forms.TextBox()
        Me.PhoneLB = New System.Windows.Forms.Label()
        Me.PhoneTB = New System.Windows.Forms.TextBox()
        Me.PeselLB = New System.Windows.Forms.Label()
        Me.PeselTB = New System.Windows.Forms.TextBox()
        Me.MailLB = New System.Windows.Forms.Label()
        Me.MailTB = New System.Windows.Forms.TextBox()
        Me.SecondNameLB = New System.Windows.Forms.Label()
        Me.SecondNameTB = New System.Windows.Forms.TextBox()
        Me.NameLB = New System.Windows.Forms.Label()
        Me.NameTB = New System.Windows.Forms.TextBox()
        Me.AddNewBtn = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.AddNewBtn)
        Me.GroupBox1.Controls.Add(Me.AddressLB)
        Me.GroupBox1.Controls.Add(Me.AddressTB)
        Me.GroupBox1.Controls.Add(Me.PhoneLB)
        Me.GroupBox1.Controls.Add(Me.PhoneTB)
        Me.GroupBox1.Controls.Add(Me.PeselLB)
        Me.GroupBox1.Controls.Add(Me.PeselTB)
        Me.GroupBox1.Controls.Add(Me.MailLB)
        Me.GroupBox1.Controls.Add(Me.MailTB)
        Me.GroupBox1.Controls.Add(Me.SecondNameLB)
        Me.GroupBox1.Controls.Add(Me.SecondNameTB)
        Me.GroupBox1.Controls.Add(Me.NameLB)
        Me.GroupBox1.Controls.Add(Me.NameTB)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(275, 289)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Nowy pacjent"
        Me.GroupBox1.UseWaitCursor = True
        '
        'AddressLB
        '
        Me.AddressLB.AutoSize = True
        Me.AddressLB.Location = New System.Drawing.Point(6, 153)
        Me.AddressLB.Name = "AddressLB"
        Me.AddressLB.Size = New System.Drawing.Size(34, 13)
        Me.AddressLB.TabIndex = 11
        Me.AddressLB.Text = "Adres"
        Me.AddressLB.UseWaitCursor = True
        '
        'AddressTB
        '
        Me.AddressTB.Location = New System.Drawing.Point(112, 150)
        Me.AddressTB.Name = "AddressTB"
        Me.AddressTB.Size = New System.Drawing.Size(157, 20)
        Me.AddressTB.TabIndex = 10
        Me.AddressTB.UseWaitCursor = True
        '
        'PhoneLB
        '
        Me.PhoneLB.AutoSize = True
        Me.PhoneLB.Location = New System.Drawing.Point(6, 127)
        Me.PhoneLB.Name = "PhoneLB"
        Me.PhoneLB.Size = New System.Drawing.Size(79, 13)
        Me.PhoneLB.TabIndex = 9
        Me.PhoneLB.Text = "Numer telefonu"
        Me.PhoneLB.UseWaitCursor = True
        '
        'PhoneTB
        '
        Me.PhoneTB.Location = New System.Drawing.Point(112, 124)
        Me.PhoneTB.Name = "PhoneTB"
        Me.PhoneTB.Size = New System.Drawing.Size(157, 20)
        Me.PhoneTB.TabIndex = 8
        Me.PhoneTB.UseWaitCursor = True
        '
        'PeselLB
        '
        Me.PeselLB.AutoSize = True
        Me.PeselLB.Location = New System.Drawing.Point(6, 101)
        Me.PeselLB.Name = "PeselLB"
        Me.PeselLB.Size = New System.Drawing.Size(41, 13)
        Me.PeselLB.TabIndex = 7
        Me.PeselLB.Text = "PESEL"
        Me.PeselLB.UseWaitCursor = True
        '
        'PeselTB
        '
        Me.PeselTB.Location = New System.Drawing.Point(112, 98)
        Me.PeselTB.Name = "PeselTB"
        Me.PeselTB.Size = New System.Drawing.Size(157, 20)
        Me.PeselTB.TabIndex = 6
        Me.PeselTB.UseWaitCursor = True
        '
        'MailLB
        '
        Me.MailLB.AutoSize = True
        Me.MailLB.Location = New System.Drawing.Point(6, 75)
        Me.MailLB.Name = "MailLB"
        Me.MailLB.Size = New System.Drawing.Size(64, 13)
        Me.MailLB.TabIndex = 5
        Me.MailLB.Text = "Adres e-mail"
        Me.MailLB.UseWaitCursor = True
        '
        'MailTB
        '
        Me.MailTB.Location = New System.Drawing.Point(112, 72)
        Me.MailTB.Name = "MailTB"
        Me.MailTB.Size = New System.Drawing.Size(157, 20)
        Me.MailTB.TabIndex = 4
        Me.MailTB.UseWaitCursor = True
        '
        'SecondNameLB
        '
        Me.SecondNameLB.AutoSize = True
        Me.SecondNameLB.Location = New System.Drawing.Point(6, 49)
        Me.SecondNameLB.Name = "SecondNameLB"
        Me.SecondNameLB.Size = New System.Drawing.Size(53, 13)
        Me.SecondNameLB.TabIndex = 3
        Me.SecondNameLB.Text = "Nazwisko"
        Me.SecondNameLB.UseWaitCursor = True
        '
        'SecondNameTB
        '
        Me.SecondNameTB.Location = New System.Drawing.Point(112, 46)
        Me.SecondNameTB.Name = "SecondNameTB"
        Me.SecondNameTB.Size = New System.Drawing.Size(157, 20)
        Me.SecondNameTB.TabIndex = 2
        Me.SecondNameTB.UseWaitCursor = True
        '
        'NameLB
        '
        Me.NameLB.AutoSize = True
        Me.NameLB.Location = New System.Drawing.Point(6, 23)
        Me.NameLB.Name = "NameLB"
        Me.NameLB.Size = New System.Drawing.Size(26, 13)
        Me.NameLB.TabIndex = 1
        Me.NameLB.Text = "Imie"
        Me.NameLB.UseWaitCursor = True
        '
        'NameTB
        '
        Me.NameTB.Location = New System.Drawing.Point(112, 20)
        Me.NameTB.Name = "NameTB"
        Me.NameTB.Size = New System.Drawing.Size(157, 20)
        Me.NameTB.TabIndex = 0
        Me.NameTB.UseWaitCursor = True
        '
        'AddNewBtn
        '
        Me.AddNewBtn.Location = New System.Drawing.Point(84, 223)
        Me.AddNewBtn.Name = "AddNewBtn"
        Me.AddNewBtn.Size = New System.Drawing.Size(106, 27)
        Me.AddNewBtn.TabIndex = 12
        Me.AddNewBtn.Text = "Dodaj pacjenta"
        Me.AddNewBtn.UseVisualStyleBackColor = True
        '
        'AddClient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(300, 307)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "AddClient"
        Me.Text = "AddClient"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents NameLB As Label
    Friend WithEvents NameTB As TextBox
    Friend WithEvents AddressLB As Label
    Friend WithEvents AddressTB As TextBox
    Friend WithEvents PhoneLB As Label
    Friend WithEvents PhoneTB As TextBox
    Friend WithEvents PeselLB As Label
    Friend WithEvents PeselTB As TextBox
    Friend WithEvents MailLB As Label
    Friend WithEvents MailTB As TextBox
    Friend WithEvents SecondNameLB As Label
    Friend WithEvents SecondNameTB As TextBox
    Friend WithEvents AddNewBtn As Button
End Class
