﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class ClientsList
    Dim selectedID As Int32 = 0
    Sub New()
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Try
            InitializeComponent()
            editBtn.Enabled = False
            ClientsLV.View = View.Details
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConString").ConnectionString)
            conn.Open()
            Dim ds As DataSet = New DataSet
            Dim sql As String = "SELECT id_client,name,secondname,mail,pesel,telephone,address FROM clients"

            Dim itemcoll(7) As String
            ClientsLV.Columns.Add("ID")
            ClientsLV.Columns.Add("Imie")
            ClientsLV.Columns.Add("Nazwisko")
            ClientsLV.Columns.Add("Adres mail")
            ClientsLV.Columns.Add("PESEL")
            ClientsLV.Columns.Add("Numer telefonu")
            ClientsLV.Columns.Add("Adres")

            cmd = New SqlCommand(sql, conn)
            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(ds, "clients")
            da.Dispose()



            For i = 0 To ds.Tables("clients").Rows.Count - 1
                For j = 0 To ds.Tables("clients").Columns.Count - 1
                    itemcoll(j) = ds.Tables("clients").Rows(i)(j).ToString()
                Next
                Dim lvi As New ListViewItem(itemcoll)
                Me.ClientsLV.Items.Add(lvi)
                Me.ClientsLV.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            Next

        Catch ex As Exception
            MessageBox.Show("Wystąpił problem przy odczytywaniu z bazy: " & ex.Message)
            Me.Close()
        End Try

    End Sub

    Private Sub ClientsLV_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ClientsLV.SelectedIndexChanged
        If ClientsLV.SelectedItems.Count > 0 Then
            selectedID = CInt(ClientsLV.SelectedIndices(0))
            editBtn.Enabled = True
        Else
            editBtn.Enabled = False
        End If
    End Sub

    Private Sub editBtn_Click(sender As Object, e As EventArgs) Handles editBtn.Click
        editBtn.Enabled = False
        Dim AddClientBox = New AddClient(ClientsLV.Items(selectedID).SubItems(0).Text, ClientsLV.Items(selectedID).SubItems(1).Text, ClientsLV.Items(selectedID).SubItems(2).Text, ClientsLV.Items(selectedID).SubItems(3).Text, ClientsLV.Items(selectedID).SubItems(4).Text, ClientsLV.Items(selectedID).SubItems(5).Text, ClientsLV.Items(selectedID).SubItems(6).Text, Me)
        AddClientBox.Show()
    End Sub

    Public Sub Update(ByVal name As String, ByVal secondName As String, ByVal mail As String, ByVal pesel As String, ByVal phone As String, ByVal address As String, ByVal clientId As Int32)
        Dim itemcoll(7) As String
        itemcoll(0) = clientId
        itemcoll(1) = name
        itemcoll(2) = secondName
        itemcoll(3) = mail
        itemcoll(4) = pesel
        itemcoll(5) = phone
        itemcoll(6) = address
        Dim lvi As New ListViewItem(itemcoll)
        Me.ClientsLV.Items(selectedID) = lvi
        Me.ClientsLV.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
    End Sub

End Class