﻿Imports System.Configuration
Imports System.Data.SqlClient
Public Class AddClient

    Private controller As ClientsList

    Private ep As New ErrorProvider

    Public Enum ActionType
        Add = 0
        Update = 1
    End Enum

    Dim type As Int16

    Dim clientId As Int32

    Public Sub New(ByVal actType As ActionType)
        type = actType
        InitializeComponent()
        SetLabels()
    End Sub

    Public Sub New(ByVal id As Int32, ByVal name As String, ByVal secondName As String, ByVal mail As String, ByVal pesel As String, ByVal phone As String, ByVal address As String, ByRef clientList As ClientsList)
        type = ActionType.Update
        clientId = id
        InitializeComponent()
        NameTB.Text = name
        SecondNameTB.Text = secondName
        MailTB.Text = mail
        PeselTB.Text = pesel
        PhoneTB.Text = phone
        AddressTB.Text = address
        SetLabels()
        controller = clientList
    End Sub

    Private Sub SetLabels()
        If type = ActionType.Update Then
            GroupBox1.Text = "Edytuj pacjenta"
            AddNewBtn.Text = "Edytuj pacjenta"
        End If
    End Sub

    Private Function ValidateData(ByVal name As String, ByVal secondName As String, ByVal mail As String, ByVal pesel As String, ByVal phone As String, ByVal address As String) As Boolean
        Dim isBlankError As Boolean = False
        Dim isValidationError As Boolean = False
        Dim errorMessage As String = ""
        If name.Length = 0 Or secondName.Length = 0 Or mail.Length = 0 Or pesel.Length = 0 Or phone.Length = 0 Or address.Length = 0 Then
            isBlankError = True
            errorMessage = "Wypełnij puste pola. "
        End If
        If mail.Length > 0 And Not CustomValid.IsEmail(mail) Then
            isValidationError = True
            errorMessage += "Wypełnij poprawnie adres e-mail"
        End If

        If pesel.Length > 0 And Not CustomValid.IsPesel(pesel) Then
            If (isValidationError) Then
                errorMessage += ", numer PESEL"
            Else
                isValidationError = True
                errorMessage += "Wypełnij poprawnie numer PESEL"
            End If
        End If

        If phone.Length > 0 And Not CustomValid.IsPhone(phone) Then
            If (isValidationError) Then
                errorMessage += ", numer telefonu"
            Else
                isValidationError = True
                errorMessage += "Wypełnij poprawnie numer telefonu"
            End If
        End If

        If isValidationError Then
            errorMessage += "."
        End If


        If isValidationError Or isBlankError Then
            MessageBox.Show(errorMessage)
            Return False
        End If
        Return True

    End Function

    Private Sub AddClientToDatabase(ByVal name As String, ByVal secondName As String, ByVal mail As String, ByVal pesel As String, ByVal phone As String, ByVal address As String)
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Try
            con.ConnectionString = ConfigurationManager.ConnectionStrings("ConString").ConnectionString
            con.Open()
            cmd.Connection = con

            Select Case type
                Case ActionType.Add
                    cmd.CommandText = String.Format("INSERT INTO clients(name, secondname, mail, pesel, telephone, address) VALUES('{0}','{1}','{2}','{3}','{4}','{5}')", name, secondName, mail, pesel, phone, address)
                    cmd.ExecuteNonQuery()
                    MessageBox.Show("Dodano nowego pacjenta")
                Case ActionType.Update
                    cmd.CommandText = String.Format("Update clients set name='{0}', secondname='{1}', mail='{2}', pesel='{3}', telephone='{4}', address='{5}' where id_client = {6}", name, secondName, mail, pesel, phone, address, clientId)
                    cmd.ExecuteNonQuery()
                    MessageBox.Show("Zaaktualizowano pacjenta")
                    controller.Update(name, secondName, mail, pesel, phone, address, clientId)
            End Select

        Catch ex As Exception
            MessageBox.Show("Wystąpił problem: " & ex.Message)
        Finally
            con.Close()
        End Try

    End Sub

    Private Sub AddNewBtn_Click(sender As Object, e As EventArgs) Handles AddNewBtn.Click

        Dim name As String = NameTB.Text
        Dim secondName As String = SecondNameTB.Text
        Dim mail As String = MailTB.Text
        Dim pesel As String = PeselTB.Text
        Dim phone As String = PhoneTB.Text
        Dim address As String = AddressTB.Text

        If ValidateData(name, secondName, mail, pesel, phone, address) Then
            AddClientToDatabase(name, secondName, mail, pesel, phone, address)
        End If

    End Sub

End Class