﻿Imports System.Text.RegularExpressions

Public Class CustomValid

    Public Shared Function IsEmail(ByVal email As String) As Boolean
        Static emailExpression As New Regex("^[_a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z]{2,4})$")

        Return emailExpression.IsMatch(email)
    End Function

    Public Shared Function IsPhone(ByVal phone As String) As Boolean
        Static phoneExpression As New Regex("^[0-9]{7,11}$")

        Return phoneExpression.IsMatch(phone)
    End Function

    Public Shared Function IsPesel(ByVal pesel As String) As Boolean
        Static peselExpression As New Regex("^[0-9]{11}$")
        Dim a, b, c, d, e, f, g, h, i, j, k, sum As Int16

        If peselExpression.IsMatch(pesel) Then

            a = CInt(Mid(pesel, 1, 1))
            b = CInt(Mid(pesel, 2, 1))
            c = CInt(Mid(pesel, 3, 1))
            d = CInt(Mid(pesel, 4, 1))
            e = CInt(Mid(pesel, 5, 1))
            f = CInt(Mid(pesel, 6, 1))
            g = CInt(Mid(pesel, 7, 1))
            h = CInt(Mid(pesel, 8, 1))
            i = CInt(Mid(pesel, 9, 1))
            j = CInt(Mid(pesel, 10, 1))
            k = CInt(Mid(pesel, 11, 1))

            sum = a * 1 + b * 3 + c * 7 + d * 9 + e * 1 + f * 3 + g * 7 + h * 9 + i * 1 + j * 3

            If (10 - sum Mod 10) Mod 10 = k Then
                Return True
            End If
        End If
        Return False
    End Function

End Class
