﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AddClientBtn = New System.Windows.Forms.Button()
        Me.ShowClientsBtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'AddClientBtn
        '
        Me.AddClientBtn.Location = New System.Drawing.Point(78, 99)
        Me.AddClientBtn.Name = "AddClientBtn"
        Me.AddClientBtn.Size = New System.Drawing.Size(124, 37)
        Me.AddClientBtn.TabIndex = 0
        Me.AddClientBtn.Text = "Dodaj pacjenta"
        Me.AddClientBtn.UseVisualStyleBackColor = True
        '
        'ShowClientsBtn
        '
        Me.ShowClientsBtn.Location = New System.Drawing.Point(78, 155)
        Me.ShowClientsBtn.Name = "ShowClientsBtn"
        Me.ShowClientsBtn.Size = New System.Drawing.Size(124, 37)
        Me.ShowClientsBtn.TabIndex = 1
        Me.ShowClientsBtn.Text = "Wyświetl pacjentów"
        Me.ShowClientsBtn.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(272, 204)
        Me.Controls.Add(Me.ShowClientsBtn)
        Me.Controls.Add(Me.AddClientBtn)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents AddClientBtn As Button
    Friend WithEvents ShowClientsBtn As Button
End Class
