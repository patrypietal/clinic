﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ClientsList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ClientsLV = New System.Windows.Forms.ListView()
        Me.editBtn = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ClientsLV
        '
        Me.ClientsLV.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ClientsLV.Location = New System.Drawing.Point(0, 19)
        Me.ClientsLV.MultiSelect = False
        Me.ClientsLV.Name = "ClientsLV"
        Me.ClientsLV.Size = New System.Drawing.Size(386, 209)
        Me.ClientsLV.TabIndex = 0
        Me.ClientsLV.UseCompatibleStateImageBehavior = False
        '
        'editBtn
        '
        Me.editBtn.AutoSize = True
        Me.editBtn.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.editBtn.Location = New System.Drawing.Point(3, 230)
        Me.editBtn.Margin = New System.Windows.Forms.Padding(30)
        Me.editBtn.MinimumSize = New System.Drawing.Size(60, 20)
        Me.editBtn.Name = "editBtn"
        Me.editBtn.Size = New System.Drawing.Size(380, 23)
        Me.editBtn.TabIndex = 1
        Me.editBtn.Text = "Edytuj"
        Me.editBtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.ClientsLV)
        Me.GroupBox1.Controls.Add(Me.editBtn)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(386, 256)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Lista pacjentów"
        '
        'ClientsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(410, 280)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "ClientsList"
        Me.Text = "ClientsList"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ClientsLV As ListView
    Friend WithEvents editBtn As Button
    Friend WithEvents GroupBox1 As GroupBox
End Class
